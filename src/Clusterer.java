public class Clusterer {

	public ClusterRow clusterStep(ClusterRow cr, ClusterMethod cm) {
		double minValue = Double.MAX_VALUE;
		int memI = 0;
		int memJ = 0;
		
		for (int i = 1 ; i < cr.getLength() ; i++) {
			for (int j = i + 1 ; j < cr.getLength() ; j++) {
				if (minValue > cm.calculateDistance(cr.getCluster(i), cr.getCluster(j))) {
					minValue = cm.calculateDistance(cr.getCluster(i), cr.getCluster(j));
					memI = i;
					memJ = j;
				}
			}
		}
		cr.placeClustersInsideNode(cr.getCluster(memI), cr.getCluster(memJ));
		return cr;
	}
}
