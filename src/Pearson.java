public class Pearson implements DistanceMeasure {

	public double calculateDistance(Unit unit1, Unit unit2) {
		double result = 0.0;
		
		for(int i = 0 ; i < unit1.getNumberOfVariables() ; i++) {
			result += ((unit1.getValue(i) - calculateMean(unit1)) / calculateDeviation(unit1)) *
					((unit2.getValue(i) - calculateMean(unit2)) / calculateDeviation(unit2));
		}
		result = result / (unit1.getNumberOfVariables() - 1);
		return 1 - result;
	}
	
	private double calculateMean(Unit unit) {
		double meanValue = 0.0;
		
		for (int value = 0; value < unit.getNumberOfVariables() ; value++) {
			meanValue += unit.getValue(value) / unit.getNumberOfVariables();
		}
		return meanValue;
	}
		
	private double calculateDeviation(Unit unit) {
		double deviationValue = 0.0;
		
		for (int value = 0; value < unit.getNumberOfVariables() ; value++) {
			deviationValue += (unit.getValue(value) - calculateMean(unit)) * (unit.getValue(value) - calculateMean(unit));
		}
		return deviationValue = Math.sqrt(deviationValue / (unit.getNumberOfVariables() - 1));
	}

}
