public class ClusterRow {
	
	Cluster[] clusters;

	ClusterRow(Dataset data) {
		clusters = new Cluster[data.getNumberOfElements()];
		for(int i = 0 ; i < data.getNumberOfElements() ; i++) {
			Leaf leaf = new Leaf(data.getUnit(i));
			clusters[i] = leaf;
		}
	}
	
	public Cluster getCluster(int i) {
		return clusters[i];
	}
	
	public void placeClustersInsideNode(Cluster j, Cluster k) {
		Cluster[] newLengthClusters = new Cluster[clusters.length - 1];
		newLengthClusters[newLengthClusters.length - 1] = new Node(j, k);
		
		int counter = 0;
		for(int i = 0 ; i < clusters.length ; i++) {
			if (clusters[i] != j && clusters[i] != k) {
				newLengthClusters[counter++] = clusters[i];
			}
		}
		
		clusters = newLengthClusters;
	}
	
	public int getLength() {
		return clusters.length;
	}
	
}
