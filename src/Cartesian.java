import ui.*;

public class Cartesian implements View {
	
	final static int SCREEN_CORRECTION = 81,
					 BULLET_WIDTH = 10,
					 BULLET_HEIGTH = 10;
	
	DrawUserInterface ui;
	private String varNameX;
	private String varNameY;
	
	Cartesian(DrawUserInterface ui, String varName1, String varName2) {
		this.ui = ui;
		this.varNameX = varName1;
		this.varNameY = varName2;
	}
	
	public void draw(ClusterRow clusters) {
		ui.clear();
		drawAxisAndLabels();
		drawElementsAndHotSpots(clusters);
		ui.showChanges();
	}
	
	private void drawElementsAndHotSpots(ClusterRow clusters) {
		for(int i = 0 ; i < clusters.getLength() ; i++) {
			Colour colour = new Colour(UIAuxiliaryMethods.getRandom(0, 255),
					UIAuxiliaryMethods.getRandom(0, 255), UIAuxiliaryMethods.getRandom(0, 255));
			
			for (int j = 0 ; j < clusters.getCluster(i).getWidth() ; j++) {
				drawOuterCircle(clusters.getCluster(i).getUnits().getUnit(j));
				drawInnerCircle(clusters.getCluster(i).getUnits().getUnit(j), colour);
				drawClusterCircles(clusters.getCluster(i), colour);
				makeHotspot(clusters.getCluster(i).getUnits().getUnit(j));
			}
		}
	}
	
	private void drawOuterCircle(Unit unit) {
		ui.drawCircle((int)(unit.getValue(0) * (ui.getWidth() - 2 * ui.getWidth()/10) + ui.getWidth()/10),
					(int)(unit.getValue(1) * (ui.getHeight() - 2 * ui.getHeight()/10 - SCREEN_CORRECTION) + ui.getHeight()/10),
					BULLET_WIDTH,
					BULLET_HEIGTH,
					new Colour(0,0,0),
					false);
	}
	
	private void drawInnerCircle(Unit unit, Colour colour) {
		ui.drawCircle((int)(unit.getValue(0) * (ui.getWidth() - 2 * ui.getWidth()/10) + ui.getWidth()/10),
					(int)(unit.getValue(1) * (ui.getHeight() - 2 * ui.getHeight()/10 - SCREEN_CORRECTION) + ui.getHeight()/10),
					BULLET_WIDTH,
					BULLET_HEIGTH,
					colour,
					true);
	}
	
	private void drawClusterCircles(Cluster cluster, Colour colour) {
		if (cluster.hasChildren() == false) {
			return;
		}
		
		double gemX = 0.0;
		double gemY = 0.0;
		double maxValueX = -Double.MAX_VALUE;
		double maxValueY = -Double.MAX_VALUE;
		
		for (int i = 0 ; i < cluster.getWidth() ; i++) {
			gemX += cluster.getUnits().getUnit(i).getValue(0) / cluster.getWidth();
			gemY += cluster.getUnits().getUnit(i).getValue(1) / cluster.getWidth();
		}
		
		for (int i = 0 ; i < cluster.getWidth() ; i++) {
			if (Math.abs(cluster.getUnits().getUnit(i).getValue(0) - gemX) > maxValueX) {
				maxValueX = Math.abs(cluster.getUnits().getUnit(i).getValue(0) - gemX);
			}
			
			if (Math.abs(cluster.getUnits().getUnit(i).getValue(1) - gemY) > maxValueY) {
				maxValueY = Math.abs(cluster.getUnits().getUnit(i).getValue(1) - gemY);
			}
		}
		
		double radius = Math.sqrt(maxValueX * maxValueX + maxValueY * maxValueY);
		
		ui.drawCircle((int)(gemX * (ui.getWidth() - 2 * ui.getWidth()/10) + ui.getWidth()/10),
				(int) (gemY * (ui.getHeight() - 2 * ui.getHeight()/10 - SCREEN_CORRECTION) + ui.getHeight()/10),
				(int) (1000 * radius), // Klopt nog niet
				(int) (1000 * radius), // Klopt nog niet
				colour,
				false);
	}
	
	private void makeHotspot(Unit unit) {
		ui.setCircleHotspot((int)(unit.getValue(0) * (ui.getWidth() - 2 * ui.getWidth()/10) + ui.getWidth()/10),
					(int)(unit.getValue(1) * (ui.getHeight() - 2 * ui.getHeight()/10 - SCREEN_CORRECTION) + ui.getHeight()/10),
					BULLET_WIDTH,
					BULLET_HEIGTH,
					unit.getElementName());
	}
	
	private void drawAxisAndLabels() {
		drawAxisX();
		drawAxisY();
		labelAxisX();
		labelAxisY();
	}
	
	private void drawAxisX() {
		ui.drawLine(ui.getWidth()/10,
				ui.getHeight()/10,
				ui.getWidth() - ui.getWidth()/10,
				ui.getHeight()/10,
				new Colour(0,0,0));
	}
	
	private void drawAxisY() {
		ui.drawLine(ui.getWidth()/10,
				ui.getHeight()/10,
				ui.getWidth()/10,
				(ui.getHeight() - ui.getHeight()/10) - SCREEN_CORRECTION,
				new Colour(0,0,0));
	}
	
	private void labelAxisX() {
		ui.drawText((ui.getWidth() - ui.getWidth()/5) - ui.getTextWidth(varNameX),
				ui.getHeight()/10 - ui.getTextHeight(varNameX),
				varNameX,
				new Colour(0,0,0));
	}
	
	private void labelAxisY() {
		ui.drawText(ui.getTextWidth(varNameX),
				(ui.getHeight() - ui.getHeight()/10) - SCREEN_CORRECTION + ui.getTextHeight(varNameX),
				varNameY,
				new Colour(0,0,0));
	}

}
