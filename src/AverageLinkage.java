public class AverageLinkage implements ClusterMethod {

	DistanceMeasure distanceMeasure;
	
	AverageLinkage(DistanceMeasure distanceMeasure) {
		this.distanceMeasure = distanceMeasure;
	}
	
	public double calculateDistance(Cluster cluster1, Cluster cluster2) {
		double avrValue = 0.0;
		
		for(int i = 0 ; i < cluster1.getWidth() ; i++) {
			for(int j = 0 ; j < cluster2.getWidth() ; j++) {
				avrValue += distanceMeasure.calculateDistance(cluster1.getUnits().getUnit(i), cluster2.getUnits().getUnit(j));
			}
		}
		return avrValue / cluster1.getWidth() * cluster2.getWidth();
	}

}
