public class NumberRow {

	private double[] values;
	private int numberOfElements;
	
	NumberRow(int length) {
		values = new double[length];
		numberOfElements = 0;
	}
	
	public void addValue(double value) {
		values[numberOfElements] = value;
		numberOfElements += 1;
	}
	
	public void changeValue(int i, double value) {
		values[i] = value;
	}
	
	public double getValue(int i) {
		return values[i];
	}
	
	public int getLength() {
		return values.length;
	}
}
