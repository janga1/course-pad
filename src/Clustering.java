import java.util.Locale;
import java.util.Scanner;
import ui.*;

public class Clustering {
	
	final static int SCREEN_WIDTH = 600,
					 SCREEN_HEIGHT = 600;
	
	DrawUserInterface ui;
	
	public Clustering() {		
		ui = UserInterfaceFactory.getDrawUI(SCREEN_WIDTH, SCREEN_HEIGHT);
		UIAuxiliaryMethods.askUserForInput();
	}
	
	private void cluster(ClusterRow clusters, Dataset dataset) {
		Clusterer cl = new Clusterer();
		Euclidean euc = new Euclidean();
		CompleteLinkage coli = new CompleteLinkage(euc);
		Dendrogram dendro = new Dendrogram(ui);
		dendro.draw(clusters);
		
		while (dataset.getNumberOfClusters() != clusters.getLength() - 1) {
			Event event = ui.getEvent();
			
			if (event.data.equals("Space")) {
				clusters = cl.clusterStep(clusters, coli);
				dendro.draw(clusters);
			}
			
		}
		
	}
	
	private NumberRow parseNumberRow(Dataset dataset, Scanner in) {
		NumberRow numberRow = new NumberRow(dataset.getNumberOfVariables());
			
		for(int i = 0; i < dataset.getNumberOfVariables(); i++) {
			numberRow.addValue(in.nextDouble());
		}
		return numberRow;
	}
	
	private Unit parseUnit(Dataset dataset, Scanner in) {
		Unit unit = new Unit(in.next());
		
		NumberRow numberRow = parseNumberRow(dataset, in);
		unit.addNumberRow(numberRow);
		
		return unit;
	}
	
	private Dataset parseUnitRow(Dataset dataset, Scanner in) {
		UnitRow unitRow = new UnitRow(dataset.getNumberOfElements());
		
		for (int i = 0; i < dataset.getNumberOfElements(); i++) {
			Unit unit = parseUnit(dataset, in);
			unitRow.addUnit(unit);
		}
		
		dataset.setUnitRow(unitRow);
		
		return dataset;
	}
	
	private Dataset parseHeaderInfo(Dataset dataset, Scanner in) {
		dataset.setNumberOfClusters(in.nextInt());
		dataset.setNumberOfElements(in.nextInt());
		dataset.setNumberOfVariables(in.nextInt());
		dataset.setElementType(in.next());
		
		for (int i = 0; i < dataset.getNumberOfVariables(); i++) {
			dataset.addToVariableName(in.next());
		}

		return dataset;
	}
	
	private Dataset parseDataset() {
		Dataset dataset = new Dataset();
		Scanner in = new Scanner(System.in);
		in.useLocale(Locale.US);
		
		dataset = parseHeaderInfo(dataset, in);
		dataset = parseUnitRow(dataset, in);
		
		in.close();
		return dataset;
	}
	
	private void start() {
		Dataset dataset = parseDataset();
		dataset.normalization();
		dataset.preselection();
		ClusterRow clusters = new ClusterRow(dataset);
		cluster(clusters, dataset);
	}
	
	public static void main(String[] args) {
		new Clustering().start();
	}
}
