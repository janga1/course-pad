public class Leaf implements Cluster {
	
	Unit unit;
	
	Leaf(Unit unit) {
		this.unit = unit;
	}
	
	public int getDepth() {
		return 0;
	}
	
	public int getWidth() {
		return 1;
	}
	
	public UnitRow getUnits() {
		UnitRow unitRow = new UnitRow(1);
		unitRow.addUnit(unit);
		return unitRow;
	}

	public boolean hasChildren() {
		return false;
	}

}
