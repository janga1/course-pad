public class Manhattan implements DistanceMeasure {

	public double calculateDistance(Unit unit1, Unit unit2) {
		double result = 0.0;
		
		for(int i = 0 ; i < unit1.getNumberOfVariables() ; i++) {
			result += Math.abs(unit1.getValue(i) - unit2.getValue(i));
		}
		return result;
	}

}
