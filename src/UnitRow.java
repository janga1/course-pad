public class UnitRow {

	private Unit[] units;
	private int numberOfRows;
		
	UnitRow(int numberOfElements) {
		units = new Unit[numberOfElements];
		numberOfRows = 0;
	}
	
	public void addUnit(Unit unit) {
		units[numberOfRows] = unit;
		numberOfRows += 1;
	}
	
	public Unit getUnit(int i) {
		return units[i];
	}
	
	public void addUnitRow(UnitRow u) {
		for(int i = 0 ; i < u.getLength() ; i++) {
			units[numberOfRows] = u.getUnit(i);
			numberOfRows += 1;
		}
	}
	
	public int getLength() {
		return units.length;
	}
	
}
